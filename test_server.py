from unittest import TestCase
from unittest.mock import patch, PropertyMock
import logging
import os

from botocore.exceptions import ClientError as Boto3Error

import server

logging.disable()


class MockConfig():
    dns = []
    firewall = []
    email = []



class Test__Receivers(TestCase):
    """Verifies that receiver functions are called as expected.
    
    Arguments to receivers are not tested explicitly. Instead, the important arguments are echoed
    by a mock receiver and the calls inferred from the global status report.
    """
    
    def setUp(self):
        
        # Mock DNS receiver
        def dns_receiver(domain, subdomain, ipv4, ipv6):
            return {'domain': domain, 'subdomain': subdomain, 'mock': True}
        
        self.dns_mock = patch('server.dns_receiver', side_effect = dns_receiver)
        self.dns_mock.start()
        
        # Mock firewall receiver
        def firewall_receiver(config, ipv4, ipv6):
            return ({'mock': True, **config})
        
        self.firewall_mock = patch('server.firewall_receiver', side_effect = firewall_receiver)
        self.firewall_mock.start()
    
    
    def tearDown(self):
        self.dns_mock.stop()
        self.firewall_mock.stop()
    
    
    @patch('server.load_config', return_value = MockConfig())
    def test__dns__none(self, config_mock):
        """DNS receiver should not be used."""
        
        status = server.handler({'ipv4': '123.123.123.123', 'ipv6': 'e38b::ef72:1ba4'})
        
        self.assertEqual(status['dns'], [])
    
    
    @patch('server.load_config', return_value = MockConfig())
    def test__dns__single(self, config_mock):
        """Invokes the DNS receiver for each configured domain."""
        
        config_mock.return_value.dns = [{'domain': 'ex.org', 'subdomain': 'sub'}]
        
        status = server.handler({'ipv4': '123.123.123.123', 'ipv6': 'e38b::ef72:1ba4'})
        
        self.assertEqual(
            status['dns'],
            [{'domain': 'ex.org', 'subdomain': 'sub', 'mock': True}],
        )
    
    
    @patch('server.load_config', return_value = MockConfig())
    def test__dns__double(self, config_mock):
        """Invokes the DNS receiver for each configured domain."""
        
        config_mock.return_value.dns = [
                {'domain': 'ex.org', 'subdomain': 'sub'},
                {'domain': 'second.com', 'subdomain': 'local'},
            ]
        
        status = server.handler({'ipv4': '123.123.123.123', 'ipv6': 'e38b::ef72:1ba4'})
        
        self.assertEqual(
            status['dns'],
            [
                {'domain': 'ex.org', 'subdomain': 'sub', 'mock': True},
                {'domain': 'second.com', 'subdomain': 'local', 'mock': True},
            ],
        )
    
    
    @patch('server.load_config', return_value = MockConfig())
    def test__firewall__none(self, config_mock):
        """Firewall receiver should not be used."""
        
        status = server.handler({'ipv4': '123.123.123.123', 'ipv6': 'e38b::ef72:1ba4'})
        
        self.assertEqual(status['firewall'], [])
    
    
    @patch('server.load_config', return_value = MockConfig())
    def test__firewall__single(self, config_mock):
        """Invokes the firewall receiver for each configured rule set."""
        
        config_mock.return_value.firewall = [{'name': 'Test', 'port': 22}]
        
        status = server.handler({'ipv4': '123.123.123.123', 'ipv6': 'e38b::ef72:1ba4'})
        
        self.assertEqual(
            status['firewall'],
            [{'mock': True, 'name': 'Test', 'port': 22}],
        )
    
    
    @patch('server.load_config', return_value = MockConfig())
    def test__firewall__double(self, config_mock):
        """Invokes the firewall receiver for each configured rule set."""
        
        config_mock.return_value.firewall = [
            {'name': 'Test', 'port': 22},
            {'id': 'sg-d452a6b74eed67e4f', 'port': 3306, 'tag': 'A Test'}
        ]
        
        status = server.handler({'ipv4': '123.123.123.123', 'ipv6': 'e38b::ef72:1ba4'})
        
        self.assertEqual(
            status['firewall'],
            [
                {'mock': True, 'name': 'Test', 'port': 22},
                {'mock': True, 'id': 'sg-d452a6b74eed67e4f', 'port': 3306, 'tag': 'A Test'},
            ],
        )



class Test__DNS_Inputs(TestCase):
    """Verifies that inputs to the DNS receiver are handled correctly."""
    
    def setUp(self):
        
        # Test settings
        domain = 'ex.org'
        subdomain = 'sub'
        self.fqdn = subdomain + '.' + domain + '.'
        
        # Configure mocking
        self.boto_patch = patch('aws.boto3')
        boto_mock = self.boto_patch.start()
        
        self.route53_mock = boto_mock.client.return_value
        self.route53_mock.list_hosted_zones.return_value = {'HostedZones': [
            {'Name': domain + '.', 'Id': 10}, {'Name': domain, 'Id': 20},  # Expect first to be matched
        ]}
    
    
    def tearDown(self):
        self.boto_patch.stop()
    
    
    def verify_expected_changes(self, changed_records):
        """Checks the updates match those expected.
        
        First checks the number of updates match, then checks if each update in turn has been
        performed. By implication, every update must have been performed and there cannot be
        extra updates.
        """
        
        self.assertEqual(
            self.route53_mock.change_resource_record_sets.call_count,
            len(changed_records),
        )
        
        for type, value in changed_records:
            with self.subTest(type = type, value = value):
                self.route53_mock.change_resource_record_sets.assert_any_call(
                    HostedZoneId = 10,
                    ChangeBatch = {'Changes': [{
                        'Action': 'UPSERT',
                        'ResourceRecordSet': {
                            'Name': self.fqdn,
                            'Type': type,
                            'ResourceRecords': [{'Value': value}],
                            'TTL': 300,
                        },
                    }]},
                )
    
    
    def test__standard(self):
        """Retrieves records for the correct HostedZoneId."""
        
        self.route53_mock.list_resource_record_sets.return_value = {'ResourceRecordSets': []}
        
        server.dns_receiver('ex.org', 'sub', None, None)
        
        self.route53_mock.list_resource_record_sets.assert_called_once()
        self.route53_mock.list_resource_record_sets.assert_called_once_with(HostedZoneId = 10)
    
    
    def test__domain__with_trailing_dot(self):
        """Retrieves records for the correct HostedZoneId."""
        
        server.dns_receiver('ex.org.', 'sub', None, None)
        
        self.route53_mock.list_resource_record_sets.assert_called_once()
        self.route53_mock.list_resource_record_sets.assert_called_once_with(HostedZoneId = 10)
    
    
    def test__subdomain__with_trailing_dot(self):
        """Adds both IPv4 and IPv6 records."""
        
        self.route53_mock.list_resource_record_sets.return_value = {'ResourceRecordSets': []}
        
        server.dns_receiver('ex.org', 'sub.', '123.123.123.123', 'e38b::ef72:1ba4')
        
        self.verify_expected_changes([('A', '123.123.123.123'), ('AAAA', 'e38b::ef72:1ba4')])
    
    
    def test__subdomain__with_domain(self):
        """Adds both IPv4 and IPv6 records."""
        
        self.route53_mock.list_resource_record_sets.return_value = {'ResourceRecordSets': []}
        
        server.dns_receiver(
            'ex.org',
            'sub.ex.org',
            '123.123.123.123',
            'e38b::ef72:1ba4',
        )
        
        self.verify_expected_changes([('A', '123.123.123.123'), ('AAAA', 'e38b::ef72:1ba4')])
    
    
    def test__subdomain__with_domain_and_trailing_dot(self):
        """Adds both IPv4 and IPv6 records."""
        
        self.route53_mock.list_resource_record_sets.return_value = {'ResourceRecordSets': []}
        
        server.dns_receiver(
            'ex.org',
            'sub.ex.org.',
            '123.123.123.123',
            'e38b::ef72:1ba4',
        )
        
        self.verify_expected_changes([('A', '123.123.123.123'), ('AAAA', 'e38b::ef72:1ba4')])



class Test__DNS_Actions(TestCase):
    """Verifies that the DNS receiver performs the expected actions."""
    
    domain = 'ex.org'
    subdomain = 'sub'

    def setUp(self):
        
        # Test settings
        self.fqdn = self.subdomain + '.' + self.domain + '.'
        
        # Mock Boto3
        self.boto_patch = patch('aws.boto3')
        boto_mock = self.boto_patch.start()
        
        self.route53_mock = boto_mock.client.return_value
        self.route53_mock.list_hosted_zones.return_value = {'HostedZones': [
            {'Name': self.domain + '.', 'Id': 10}, {'Name': self.domain, 'Id': 20},
        ]}
    
    
    def tearDown(self):
        self.boto_patch.stop()
    
    
    def add_existing_records(self, records):
        """Mocks existing DNS records for Signpost to interact with."""
        
        self.route53_mock.list_resource_record_sets.return_value = {'ResourceRecordSets': [{
            'Name': self.fqdn,
            'Type': type,
            'ResourceRecords': [{'Value': value}],
            'TTL': 300,
        } for type, value in records]}
    
    
    def verify_expected_changes(self, changed_records):
        """Checks the updates match those expected.
        
        First checks the number of updates match, then checks if each update in turn has been
        performed. By implication, every update must have been performed and there cannot be
        extra updates.
        """
        
        self.assertEqual(
            self.route53_mock.change_resource_record_sets.call_count,
            len(changed_records),
        )
        
        for type, value in changed_records:
            with self.subTest(type = type, value = value):
                self.route53_mock.change_resource_record_sets.assert_any_call(
                    HostedZoneId = 10,
                    ChangeBatch = {'Changes': [{
                        'Action': 'UPSERT',
                        'ResourceRecordSet': {
                            'Name': self.fqdn,
                            'Type': type,
                            'ResourceRecords': [{'Value': value}],
                            'TTL': 300,
                        },
                    }]},
                )
    
    
    def test__no_data(self):
        """Performs no updates."""
        
        report = server.dns_receiver(self.domain, self.subdomain, None, None)
        
        self.route53_mock.change_resource_record_sets.assert_not_called()
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4__add(self):
        """Only adds an IPv4 record."""
        
        report = server.dns_receiver(self.domain, self.subdomain, '123.123.123.123', None)
        
        self.verify_expected_changes([('A', '123.123.123.123')])
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': True, 'ipv4_update_success': True,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4__update(self):
        """Only updates the IPv4 record."""
        
        self.add_existing_records([('A', '0.0.0.0')])
        
        report = server.dns_receiver(self.domain, self.subdomain, '123.123.123.123', None)
        
        self.verify_expected_changes([('A', '123.123.123.123')])
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': True, 'ipv4_update_success': True,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4__no_change(self):
        """Performs no updates."""
        
        self.add_existing_records([('A', '123.123.123.123')])
        
        report = server.dns_receiver(self.domain, self.subdomain, '123.123.123.123', None)
        
        self.route53_mock.change_resource_record_sets.assert_not_called()
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': False, 'ipv4_update_success': None,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4__update_error(self):
        """Safely handles the error and reports the failure."""
        
        self.add_existing_records([('A', '0.0.0.0')])
        
        self.route53_mock.change_resource_record_sets.side_effect = Boto3Error(
            {'Error': {'Code': 'InvalidChangeBatch'}},
            'ChangeResourceRecordSets',
        )
        
        report = server.dns_receiver(self.domain, self.subdomain, '123.123.123.123', None)
        
        self.verify_expected_changes([('A', '123.123.123.123')])
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': True, 'ipv4_update_success': False,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv6__add(self):
        """Only adds an IPv6 record."""
        
        report = server.dns_receiver(self.domain, self.subdomain, None, 'e38b::ef72:1ba4')
        
        self.verify_expected_changes([('AAAA', 'e38b::ef72:1ba4')])
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': True, 'ipv6_update_success': True,
            },
        )
    
    
    def test__ipv6__update(self):
        """Only updates the IPv6 record."""
        
        self.add_existing_records([('AAAA', '::1')])
        
        report = server.dns_receiver(self.domain, self.subdomain, None, 'e38b::ef72:1ba4')
        
        self.verify_expected_changes([('AAAA', 'e38b::ef72:1ba4')])
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': True, 'ipv6_update_success': True,
            },
        )
    
    
    def test__ipv6__no_change(self):
        """Performs no updates."""
        
        self.add_existing_records([('AAAA', 'e38b::ef72:1ba4')])
        
        report = server.dns_receiver(self.domain, self.subdomain, None, 'e38b::ef72:1ba4')
        
        self.route53_mock.change_resource_record_sets.assert_not_called()
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': False, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv6__update_error(self):
        """Safely handles the error and reports the failure."""
        
        self.add_existing_records([('AAAA', '::1')])
        
        self.route53_mock.change_resource_record_sets.side_effect = Boto3Error(
            {'Error': {'Code': 'InvalidChangeBatch'}},
            'ChangeResourceRecordSets',
        )
        
        report = server.dns_receiver(self.domain, self.subdomain, None, 'e38b::ef72:1ba4')
        
        self.verify_expected_changes([('AAAA', 'e38b::ef72:1ba4')])
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': True, 'ipv6_update_success': False,
            },
        )
    
    
    def test__both__add(self):
        """Adds both IPv4 and IPv6 records."""
        
        report = server.dns_receiver(
            self.domain,
            self.subdomain,
            '123.123.123.123',
            'e38b::ef72:1ba4',
        )
        
        self.verify_expected_changes([('A', '123.123.123.123'), ('AAAA', 'e38b::ef72:1ba4')])
        self.assertEqual(
            report,
            {
                'target_domain': self.fqdn,
                'ipv4_update': True, 'ipv4_update_success': True,
                'ipv6_update': True, 'ipv6_update_success': True,
            },
        )



class Test__Firewall_Actions(TestCase):
    """Verifies the firewall receiver performs the expected actions."""
    
    # Test settings
    port = 3306
    description = 'Managed by Signpost'
    
    def setUp(self):
        
        # Mock Boto3
        self.boto_patch = patch('aws.boto3')
        self.boto_mock = self.boto_patch.start()
        
        self.sg_mock = self.boto_mock.resource.return_value.SecurityGroup.return_value
        self.sg_mock.group_id = 'sg-d452a6b74eed67e4f'
        self.sg_mock.group_name = 'TestGroupName'
        self.sg_mock.ip_permissions = []
    
    
    def tearDown(self):
        self.boto_patch.stop()
    
    
    def add_existing_rules(self, ips = [], description = None):
        """Mocks existing firewall rules for Signpost to interact with."""
        
        existing_rules = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'IpRanges': [],
            'Ipv6Ranges': [],
            'PrefixListIds': [],
            'UserIdGroupPairs': [],
        }]
        for ip in ips:
            
            ipv6 = ':' in ip
            ip_range_key = 'Ipv6Ranges' if ipv6 else 'IpRanges'
            cidr_ip_key = 'CidrIpv6' if ipv6 else 'CidrIp'
            cidr_subnet = '/128' if ipv6 else '/32'
            
            existing_rules[0][ip_range_key].append({
                cidr_ip_key: ip + (cidr_subnet if '/' not in ip else ''),
                'Description': description or self.description,
            })
        
        self.sg_mock.ip_permissions.extend(existing_rules)
    
    
    def verify_rule_additions(self, additions, tag = None):
        """Checks the rules added match those expected.
        
        First checks the number of additions match, then checks in turn for each individual
        addition. By implication, every new expected rule must have been added and there cannot be
        extra rules added.
        """
        
        description = self.description + (' ({})'.format(tag) if tag else '')
        
        self.assertEqual(
            self.sg_mock.authorize_ingress.call_count,
            len(additions),
        )
        
        for ip in additions:
            with self.subTest(ip = ip):
                
                ipv6 = ':' in ip
                ip_range_key = 'Ipv6Ranges' if ipv6 else 'IpRanges'
                cidr_ip_key = 'CidrIpv6' if ipv6 else 'CidrIp'
                cidr_subnet = '/128' if ipv6 else '/32'
                
                self.sg_mock.authorize_ingress.assert_any_call(IpPermissions = [{
                    'FromPort': self.port,
                    'ToPort': self.port,
                    'IpProtocol': 'tcp',
                    ip_range_key: [{
                        cidr_ip_key: ip + cidr_subnet,
                        'Description': description,
                    }]
                }])
    
    
    def test__config__id(self):
        """Loads the security group with the matching ID."""
        
        server.firewall_receiver({'id': 'sg-d452a6b74eed67e4f', 'port': self.port}, None, None)
        self.boto_mock.resource.return_value.SecurityGroup.assert_called_once_with('sg-d452a6b74eed67e4f')
    
    
    def test__config__no_matching_name(self):
        """Returns (but does not raise) and error message."""
        
        self.boto_mock.client.return_value.describe_security_groups.side_effect = Boto3Error(
            {'Error': {'Code': 'InvalidGroup.NotFound'}},
            'DescribeSecurityGroups',
        )
        
        report = server.firewall_receiver({'name': 'TestGroupName', 'port': self.port}, None, None)
        
        self.assertEqual(
            report,
            {'name': 'TestGroupName', 'port': self.port, 'error': 'Unable to uniquely find security group.'},
        )
    
    
    def test__config__single_matching_name(self):
        """Loads the security group that matches the name."""
        
        self.boto_mock.client.return_value.describe_security_groups.return_value = {
            'SecurityGroups': [{'GroupId': 'sg-d452a6b74eed67e4f'}],
        }
        
        server.firewall_receiver({'name': 'TestGroupName', 'port': self.port}, None, None)
        
        self.boto_mock.resource.return_value.SecurityGroup.assert_called_once_with('sg-d452a6b74eed67e4f')
    
    
    def test__ipv4_add__exists_unmanaged(self):
        """Does not attempt to duplicate unmanaged rule."""
        
        self.add_existing_rules(['123.123.123.123'], description = 'Some Comment')
        
        report = server.firewall_receiver({'port': self.port}, '123.123.123.123', None)
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.sg_mock.authorize_ingress.assert_not_called()
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': False, 'ipv4_update_success': None,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4_add__exists_managed(self):
        """Performs no actions."""
        
        self.add_existing_rules(['123.123.123.123'])
        
        report = server.firewall_receiver({'port': self.port}, '123.123.123.123', None)
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.sg_mock.authorize_ingress.assert_not_called()
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': False, 'ipv4_update_success': None,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4_add__new(self):
        """Adds an IPv4 rule."""
        
        report = server.firewall_receiver({'port': self.port}, '123.123.123.123', None)
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.verify_rule_additions(['123.123.123.123'])
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': True, 'ipv4_update_success': True,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4_add__error(self):
        """Safely handles the error and reports the failure."""
        
        self.sg_mock.authorize_ingress.side_effect = Boto3Error(
            {'Error': {'Code': 'InvalidPermission.Duplicate'}},
            'AuthorizeSecurityGroupIngress',
        )
        
        report = server.firewall_receiver({'port': self.port}, '123.123.123.123', None)
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.verify_rule_additions(['123.123.123.123'])  # Checks call made ONLY
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': True, 'ipv4_update_success': False,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4_remove__with_add(self):
        """Adds the new IPv4 rule and removes any existing rules."""
        
        self.add_existing_rules(['0.0.0.0', '1.2.3.4'])
        
        report = server.firewall_receiver({'port': self.port}, '123.123.123.123', None)
        
        self.sg_mock.revoke_ingress.assert_called_once_with(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'IpRanges': [{'CidrIp': '0.0.0.0/32'}, {'CidrIp': '1.2.3.4/32'}],
        }])
        self.verify_rule_additions(['123.123.123.123'])
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': True, 'ipv4_update_success': True,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4_remove__with_unmanaged(self):
        """Removes any existing rules managed rules."""
        
        self.add_existing_rules(['0.0.0.0', '1.2.3.4'])
        self.add_existing_rules(['123.123.123.123'], description = 'Some Comment')
        
        report = server.firewall_receiver({'port': self.port}, '123.123.123.123', None)
        
        self.sg_mock.revoke_ingress.assert_called_once_with(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'IpRanges': [{'CidrIp': '0.0.0.0/32'}, {'CidrIp': '1.2.3.4/32'}],
        }])
        self.sg_mock.authorize_ingress.assert_not_called()
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': True, 'ipv4_update_success': True,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv4_remove__error(self):
        """Safely handles the error and reports the failure."""
        
        self.sg_mock.revoke_ingress.side_effect = Boto3Error(
            {'Error': {'Code': 'InvalidPermission.Duplicate'}},
            'AuthorizeSecurityGroupIngress',
        )
        
        self.add_existing_rules(['0.0.0.0', '1.2.3.4'])
        
        report = server.firewall_receiver({'port': self.port}, '123.123.123.123', None)
        
        self.sg_mock.revoke_ingress.assert_called_once_with(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'IpRanges': [{'CidrIp': '0.0.0.0/32'}, {'CidrIp': '1.2.3.4/32'}],
        }])
        self.verify_rule_additions(['123.123.123.123'])  # Checks call made ONLY
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': True, 'ipv4_update_success': False,
                'ipv6_update': None, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv6_add__exists_unmanaged(self):
        """Does not attempt to duplicate unmanaged rule."""
        
        self.add_existing_rules(['e38b::ef72:1ba4'], description = 'Some Comment')
        
        report = server.firewall_receiver({'port': self.port}, None, 'e38b::ef72:1ba4')
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.sg_mock.authorize_ingress.assert_not_called()
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': False, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv6_add__exists_managed(self):
        """Performs no actions."""
        
        self.add_existing_rules(['e38b::ef72:1ba4'])
        
        report = server.firewall_receiver({'port': self.port}, None, 'e38b::ef72:1ba4')
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.sg_mock.authorize_ingress.assert_not_called()
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': False, 'ipv6_update_success': None,
            },
        )
    
    
    def test__ipv6_add__new(self):
        """Adds an IPv6 rule."""
        
        report = server.firewall_receiver({'port': self.port}, None, 'e38b::ef72:1ba4')
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.verify_rule_additions(['e38b::ef72:1ba4'])
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': True, 'ipv6_update_success': True,
            },
        )
    
    
    def test__ipv6_add__error(self):
        """Safely handles the error and reports the failure."""
        
        self.sg_mock.authorize_ingress.side_effect = Boto3Error(
            {'Error': {'Code': 'InvalidPermission.Duplicate'}},
            'AuthorizeSecurityGroupIngress',
        )
        
        report = server.firewall_receiver({'port': self.port}, None, 'e38b::ef72:1ba4')
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.verify_rule_additions(['e38b::ef72:1ba4'])  # Checks call made ONLY
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': True, 'ipv6_update_success': False,
            },
        )
    
    
    def test__ipv6_remove__with_add(self):
        """Adds the new IPv6 rule and removes any existing rules."""
        
        self.add_existing_rules(['::1', '::2'])
        
        report = server.firewall_receiver({'port': self.port}, None, 'e38b::ef72:1ba4')
        
        self.sg_mock.revoke_ingress.assert_called_once_with(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'Ipv6Ranges': [{'CidrIpv6': '::1/128'}, {'CidrIpv6': '::2/128'}],
        }])
        self.verify_rule_additions(['e38b::ef72:1ba4'])
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': True, 'ipv6_update_success': True,
            },
        )
    
    
    def test__ipv6_remove__with_unmanaged(self):
        """Removes any existing rules managed rules."""
        
        self.add_existing_rules(['::1', '::2'])
        self.add_existing_rules(['e38b::ef72:1ba4'], description = 'Some Comment')
        
        report = server.firewall_receiver({'port': self.port}, None, 'e38b::ef72:1ba4')
        
        self.sg_mock.revoke_ingress.assert_called_once_with(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'Ipv6Ranges': [{'CidrIpv6': '::1/128'}, {'CidrIpv6': '::2/128'}],
        }])
        self.sg_mock.authorize_ingress.assert_not_called()
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': True, 'ipv6_update_success': True,
            },
        )
    
    
    def test__ipv6_remove__error(self):
        """Safely handles the error and reports the failure."""
        
        self.sg_mock.revoke_ingress.side_effect = Boto3Error(
            {'Error': {'Code': 'InvalidPermission.Duplicate'}},
            'AuthorizeSecurityGroupIngress',
        )
        
        self.add_existing_rules(['::1', '::2'])
        
        report = server.firewall_receiver({'port': self.port}, None, 'e38b::ef72:1ba4')
        
        self.sg_mock.revoke_ingress.assert_called_once_with(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'Ipv6Ranges': [{'CidrIpv6': '::1/128'}, {'CidrIpv6': '::2/128'}],
        }])
        self.verify_rule_additions(['e38b::ef72:1ba4'])
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': None, 'ipv4_update_success': None,
                'ipv6_update': True, 'ipv6_update_success': False,
            },
        )
    
    
    def test__both__add(self):
        """Adds both IPv4 and IPv6 rules."""
        
        report = server.firewall_receiver({'port': self.port}, '123.123.123.123', 'e38b::ef72:1ba4')
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.verify_rule_additions(['123.123.123.123', 'e38b::ef72:1ba4'])
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': True, 'ipv4_update_success': True,
                'ipv6_update': True, 'ipv6_update_success': True,
            },
        )
    
    
    def test__ignore_others(self):
        """Ignores rules for different ports or without the standard description."""
        
        different_description = {
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'IpRanges': [{'CidrIp': '0.0.0.0', 'Description': 'Not removed'}],
            'Ipv6Ranges': [{'CidrIpv6': '::1', 'Description': 'Not removed'}],
            'PrefixListIds': [],
            'UserIdGroupPairs': [],
        }
        different_port = {
            'FromPort': self.port + 2000,
            'ToPort': self.port + 2000,
            'IpProtocol': 'tcp',
            'IpRanges': [{'CidrIp': '1.2.3.4', 'Description': 'Managed by Signpost'}],
            'Ipv6Ranges': [{'CidrIpv6': '::2', 'Description': 'Managed by Signpost'}],
            'PrefixListIds': [],
            'UserIdGroupPairs': [],
        }
        
        self.sg_mock.ip_permissions = [different_description, different_port]
        
        report = server.firewall_receiver({'port': self.port}, '123.123.123.123', 'e38b::ef72:1ba4')
        
        self.sg_mock.revoke_ingress.assert_not_called()
        self.verify_rule_additions(['123.123.123.123', 'e38b::ef72:1ba4'])
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': True, 'ipv4_update_success': True,
                'ipv6_update': True, 'ipv6_update_success': True,
            },
        )
    
    
    def test__tags(self):
        """Can include a server tag in the rules being managed."""
        
        self.sg_mock.ip_permissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'IpRanges': [
                {'CidrIp': '0.0.0.0/32', 'Description': 'Managed by Signpost'},
                {'CidrIp': '1.2.3.4/32', 'Description': 'Managed by Signpost (Tagged)'},
            ],
            'Ipv6Ranges': [
                {'CidrIpv6': '::1/128', 'Description': 'Managed by Signpost'},
                {'CidrIpv6': '::2/128', 'Description': 'Managed by Signpost (Tagged)'},
            ],
            'PrefixListIds': [],
            'UserIdGroupPairs': [],
        }]
        
        report = server.firewall_receiver(
            {'port': self.port, 'tag': 'Tagged'},
            '123.123.123.123',
            'e38b::ef72:1ba4',
        )
        
        self.assertEqual(self.sg_mock.revoke_ingress.call_count, 2)
        self.sg_mock.revoke_ingress.assert_any_call(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'IpRanges': [{'CidrIp': '1.2.3.4/32'}],
        }])
        self.sg_mock.revoke_ingress.assert_any_call(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            'Ipv6Ranges': [{'CidrIpv6': '::2/128'}],
        }])
        
        self.verify_rule_additions(['123.123.123.123', 'e38b::ef72:1ba4'], tag = 'Tagged')
        self.assertEqual(
            report,
            {
                'id': 'sg-d452a6b74eed67e4f', 'name': 'TestGroupName', 'port': self.port,
                'ipv4_update': True, 'ipv4_update_success': True,
                'ipv6_update': True, 'ipv6_update_success': True,
            },
        )



class Test__Reporters(TestCase):
    """Verifies that reporting functions are called as expected."""
    
    @patch('server.email_reporter')
    @patch('server.load_config', return_value = MockConfig())
    def test__none(self, config_mock, email_mock):
        """Email reporter should not be used."""
        
        server.handler({'ipv4': '123.123.123.123', 'ipv6': 'e38b::ef72:1ba4'})
        email_mock.assert_not_called()
    
    
    @patch('server.email_reporter')
    @patch('server.load_config', return_value = MockConfig())
    def test__email__single(self, config_mock, email_mock):
        """Invokes the email reporter for each message configuration."""
        
        email_config = {'mock_email_settings': True}
        config_mock.return_value.email = [email_config]
        
        status = server.handler({'ipv4': '123.123.123.123', 'ipv6': 'e38b::ef72:1ba4'})
        email_mock.assert_called_once_with(email_config, status)
    
    
    @patch('server.email_reporter')
    @patch('server.load_config', return_value = MockConfig())
    def test__email__double(self, config_mock, email_mock):
        """Invokes the email reporter for each message configuration."""
        
        email_config_1 = {'mock_email_settings': 1}
        email_config_2 = {'mock_email_settings': 2}
        config_mock.return_value.email = [email_config_1, email_config_2]
        
        status = server.handler({'ipv4': '123.123.123.123', 'ipv6': 'e38b::ef72:1ba4'})
        self.assertEqual(email_mock.call_count, 2)
        email_mock.assert_any_call(email_config_1, status)
        email_mock.assert_any_call(email_config_2, status)



class Test__Email(TestCase):
    
    config = {
        'sender_tag': 'Test',
        'sender_address': 'signpost@example.org',
        'sender_arn': 'arn:aws:ses:...',
        'recipient': 'admin@example.org',
    }
    
    def setUp(self):
        
        # Set data
        self.status = {
            'ipv4': '192.215.34.42',
            'ipv6': '6c11:115d:7a66:eb00:b9e7:3a46:c7f:2a02',
            'dns': [],
            'firewall': [],
        }
        
        # Prepare mocking
        self.boto_patch = patch('boto3.client')
        self.boto_mock = self.boto_patch.start()
        self.send_email_mock = self.boto_mock.return_value.send_email
    
    
    def tearDown(self):
        self.boto_patch.stop()
    
    
    def test__no_receivers(self):
        """Does not send a message if no receiver arrays are empty."""
        
        server.email_reporter(self.config, self.status)
        self.assertEqual(self.send_email_mock.call_count, 0)
    
    
    def test__no_updates(self):
        """Does not send a message if no updates are performed."""
        
        self.status['dns'] = [{
            'target_domain': 'sub.example.org',
            'ipv4_update': False, 'ipv4_update_success': False,
            'ipv6_update': False, 'ipv6_update_success': False,
        }]
        self.status['firewall'] = [{
            'name': 'RemoteAccess', 'port': 2222,
            'ipv4_update': False, 'ipv4_update_success': False,
            'ipv6_update': False, 'ipv6_update_success': False,
        }]
        
        server.email_reporter(self.config, self.status)
        self.assertEqual(self.send_email_mock.call_count, 0)
    
    
    def test__header__success(self):
        """Generates the message header and settings from the configuration provided."""
        
        self.status['dns'] = [{
            'target_domain': 'sub.example.org',
            'ipv4_update': True, 'ipv4_update_success': True,
            'ipv6_update': True, 'ipv6_update_success': True,
        }]
        server.email_reporter(self.config, self.status)
        
        self.assertEqual(self.send_email_mock.call_count, 1)
        send_email_kwargs = self.send_email_mock.call_args.kwargs
        
        self.assertEqual(
            send_email_kwargs['Source'],
            'Signpost ({}) <{}>'.format(self.config['sender_tag'], self.config['sender_address']),
        )
        self.assertEqual(send_email_kwargs['SourceArn'], self.config['sender_arn'])
        self.assertEqual(
            send_email_kwargs['Destination'],
            {'ToAddresses': [self.config['recipient']]},
        )
        self.assertEqual(
            send_email_kwargs['Message']['Subject'],
            {'Data': 'Signpost Update Report'},
        )
        
        content = send_email_kwargs['Message']['Body']['Text']['Data']
        self.assertIn(
            'Signpost update performed for {}'.format(self.config['sender_tag']),
            content,
        )
        self.assertIn('IPv4: {}'.format(self.status['ipv4']), content)
        self.assertIn('IPv6: {}'.format(self.status['ipv6']), content)
    
    
    def test__header__with_fail(self):
        """Alters the subject line to highlight failures."""
        
        self.status['dns'] = [{
            'target_domain': 'sub.example.org',
            'ipv4_update': True, 'ipv4_update_success': False,
            'ipv6_update': True, 'ipv6_update_success': False,
        }]
        server.email_reporter(self.config, self.status)
        
        self.assertEqual(self.send_email_mock.call_count, 1)
        self.assertEqual(
            self.send_email_mock.call_args.kwargs['Message']['Subject'],
            {'Data': 'Signpost Update FAILED'},
        )
    
    
    def test__notices__success(self):
        """Includes a note of success for each updated resoure."""
        
        self.status['dns'] = [{
            'target_domain': 'sub.example.org',
            'ipv4_update': True, 'ipv4_update_success': True,
            'ipv6_update': True, 'ipv6_update_success': True,
        }]
        self.status['firewall'] = [{
            'name': 'RemoteAccess', 'port': 2222,
            'ipv4_update': True, 'ipv4_update_success': True,
            'ipv6_update': True, 'ipv6_update_success': True,
        }]
        
        server.email_reporter(self.config, self.status)
        self.assertEqual(self.send_email_mock.call_count, 1)
        
        content = self.send_email_mock.call_args.kwargs['Message']['Body']['Text']['Data']
        self.assertIn('sub.example.org A: Updated', content)
        self.assertIn('sub.example.org AAAA: Updated', content)
        self.assertIn('RemoteAccess (2222) IPv4: Updated', content)
        self.assertIn('RemoteAccess (2222) IPv6: Updated', content)
    
    
    def test__notices__failure(self):
        """Includes a failure notice for each unsuccessful update."""
        
        self.status['dns'] = [{
            'target_domain': 'sub.example.org',
            'ipv4_update': True, 'ipv4_update_success': False,
            'ipv6_update': True, 'ipv6_update_success': False,
        }]
        self.status['firewall'] = [{
            'name': 'RemoteAccess', 'port': 2222,
            'ipv4_update': True, 'ipv4_update_success': False,
            'ipv6_update': True, 'ipv6_update_success': False,
        }]
        
        server.email_reporter(self.config, self.status)
        self.assertEqual(self.send_email_mock.call_count, 1)
        
        content = self.send_email_mock.call_args.kwargs['Message']['Body']['Text']['Data']
        self.assertIn('sub.example.org A: Update FAILED', content)
        self.assertIn('sub.example.org AAAA: Update FAILED', content)
        self.assertIn('RemoteAccess (2222) IPv4: Update FAILED', content)
        self.assertIn('RemoteAccess (2222) IPv6: Update FAILED', content)
    
    
    def test__notices__dns_no_change(self):
        """Includes a no-action notice for each unchanged DNS resource."""
        
        self.status['dns'] = [{
            'target_domain': 'sub.example.org',
            'ipv4_update': False, 'ipv4_update_success': None,
            'ipv6_update': False, 'ipv6_update_success': None,
        }]
        self.status['firewall'] = [{
            'name': 'RemoteAccess', 'port': 2222,
            'ipv4_update': True, 'ipv4_update_success': True,
            'ipv6_update': True, 'ipv6_update_success': True,
        }]
        
        server.email_reporter(self.config, self.status)
        self.assertEqual(self.send_email_mock.call_count, 1)
        
        content = self.send_email_mock.call_args.kwargs['Message']['Body']['Text']['Data']
        self.assertIn('sub.example.org A: No change', content)
        self.assertIn('sub.example.org AAAA: No change', content)
    
    
    def test__notices___firewall_no_change(self):
        """Includes a no-action notice for each unchanged firewall resource."""
        
        self.status['dns'] = [{
            'target_domain': 'sub.example.org',
            'ipv4_update': True, 'ipv4_update_success': True,
            'ipv6_update': True, 'ipv6_update_success': True,
        }]
        self.status['firewall'] = [{
            'name': 'RemoteAccess', 'port': 2222,
            'ipv4_update': False, 'ipv4_update_success': None,
            'ipv6_update': False, 'ipv6_update_success': None,
        }]
        
        server.email_reporter(self.config, self.status)
        self.assertEqual(self.send_email_mock.call_count, 1)
        
        content = self.send_email_mock.call_args.kwargs['Message']['Body']['Text']['Data']
        self.assertIn('RemoteAccess (2222) IPv4: No change', content)
        self.assertIn('RemoteAccess (2222) IPv6: No change', content)

