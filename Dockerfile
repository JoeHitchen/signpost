FROM python:3.11-slim

RUN useradd signpost
WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY --chown=signpost requirements.txt .
RUN pip install -r requirements.txt

COPY --chown=signpost . .

USER signpost
CMD ["python","client.py"]
