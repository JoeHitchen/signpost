"""This has no external dependencies, since boto3 is already available to Lambda functions."""
import os
import logging

from botocore.exceptions import ClientError as Boto3Error

from aws import Route53SubDomain, FirewallPort, send_email

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def load_config():
    """This method allows easy mocking of the server configuration."""
    import config
    return config


def dns_receiver(domain, subdomain, ipv4, ipv6):
    """Updates the DNS records for a single domain/subdomain combination."""
    
    # Get existing DNS record
    record = Route53SubDomain(domain, subdomain)
    
    # Perform IPv4 update
    ipv4_update = ipv4 and not ipv4 == record.ipv4
    ipv4_update_success = None
    
    if ipv4_update:
        try:
            record.ipv4 = ipv4
            ipv4_update_success = True
        except Boto3Error as err:
            logging.error('{}: IPv4 update failed'.format(record.fqdn))
            logging.error(err)
            ipv4_update_success = False
    
    
    # Perform IPv6 update
    ipv6_update = ipv6 and not ipv6 == record.ipv6
    ipv6_update_success = None
    
    if ipv6_update:        
        try:
            record.ipv6 = ipv6
            ipv6_update_success = True
        except Boto3Error as err:
            logging.error('{}: IPv6 update failed'.format(record.fqdn))
            logging.error(err)
            ipv6_update_success = False
    
    return {
        'target_domain': record.fqdn,
        'ipv4_update': ipv4_update, 'ipv4_update_success': ipv4_update_success,
        'ipv6_update': ipv6_update, 'ipv6_update_success': ipv6_update_success,
    }


def firewall_receiver(config, ipv4, ipv6):
    """Updates rules on firewall for the specified port."""
    
    # Process inputs
    cidr_ipv4 = ipv4 + '/32' if ipv4 else None
    cidr_ipv6 = ipv6 + '/128' if ipv6 else None
    description = 'Managed by Signpost'
    if config.get('tag', None):
        description += ' ({})'.format(config['tag'])
    
    # Get security group
    try:
        firewall_port = FirewallPort(config)            
    except Boto3Error as err:
        logging.error('Firewall update failed')
        logging.error(err)
        return {'error': 'Unable to uniquely find security group.', **config}
    
    
    # Perform IPv4 addition
    ipv4_ranges_all = [rule['CidrIp'] for rule in firewall_port.ipv4_rules]
    ipv4_addition = ipv4 and cidr_ipv4 not in ipv4_ranges_all
    ipv4_addition_failure = False
    
    if ipv4_addition:
        try:
            firewall_port.add_cidr(cidr_ipv4, description)
        
        except Boto3Error as err:
            logging.error('{} ({}): IPv4 addition failed'.format(
                firewall_port.name,
                firewall_port.port,
            ))
            logging.error(err)
            ipv4_addition_failure = True
    
    # Perform IPv4 removals
    ipv4_removal_failure = False
    ipv4_removals = [
        rule['CidrIp'] for rule in firewall_port.ipv4_rules
        if rule['Description'] == description and not rule['CidrIp'] == cidr_ipv4
    ]
    
    try:
        firewall_port.revoke_multiple_cidr(ipv4_removals)
    
    except Boto3Error as err:
        logging.error('{} ({}): IPv4 removals failed'.format(
            firewall_port.name,
            firewall_port.port,
        ))
        logging.error(err)
        ipv4_removal_failure = True
    
    
    # Perform IPv6 addition
    ipv6_ranges_all = [rule['CidrIpv6'] for rule in firewall_port.ipv6_rules]
    ipv6_addition = ipv6 and cidr_ipv6 not in ipv6_ranges_all
    ipv6_addition_failure = False
    
    if ipv6_addition:
        try:
            firewall_port.add_cidr(cidr_ipv6, description)
        
        except Boto3Error as err:
            logging.error('{} ({}): IPv6 addition failed'.format(
                firewall_port.name,
                firewall_port.port,
            ))
            logging.error(err)
            ipv6_addition_failure = True
    
    # Perform IPv6 removals
    ipv6_removal_failure = False
    ipv6_removals = [
        rule['CidrIpv6'] for rule in firewall_port.ipv6_rules
        if rule['Description'] == description and not rule['CidrIpv6'] == cidr_ipv6
    ]
    
    try:
        firewall_port.revoke_multiple_cidr(ipv6_removals)
    
    except Boto3Error as err:
        logging.error('{} ({}): IPv6 removals failed'.format(
            firewall_port.name,
            firewall_port.port,
        ))
        logging.error(err)
        ipv6_removal_failure = True
    
    
    # Generate response
    ipv4_update = ipv4 and (ipv4_addition or bool(ipv4_removals))
    if ipv4_update:
        ipv4_update_success = not (ipv4_addition_failure or ipv4_removal_failure)
    else:
        ipv4_update_success = None
    
    ipv6_update = ipv6 and (ipv6_addition or bool(ipv6_removals))
    if ipv6_update:
        ipv6_update_success = not (ipv6_addition_failure or ipv6_removal_failure)
    else:
        ipv6_update_success = None
    
    return {
        'id': firewall_port.id,
        'name': firewall_port.name,
        'port': firewall_port.port,
        'ipv4_update': ipv4_update,
        'ipv4_update_success': ipv4_update_success,
        'ipv6_update': ipv6_update,
        'ipv6_update_success': ipv6_update_success,
    }


def email_reporter(config, response):
    """Sends an e-mail report of the update results."""
    
    def update_text(update, update_success):
        """Renders text describing an update status."""
        if not update:
            return 'No change'
        return 'Updated' if update_success else 'Update FAILED'
    
    
    # Preliminary sweep of results
    has_updates = False
    has_failures = False
    for obj in [*response['dns'], *response['firewall']]:
        has_updates = has_updates or obj['ipv4_update'] or obj['ipv6_update']
        has_failures = has_failures or (obj['ipv4_update'] and not obj['ipv4_update_success'])
        has_failures = has_failures or (obj['ipv6_update'] and not obj['ipv6_update_success'])
    
    if not has_updates:
        return None
    
    
    # Generate message content
    message = [
        'Signpost update performed for {}'.format(config['sender_tag']),
        '    IPv4: {}'.format(response['ipv4']),
        '    IPv6: {}'.format(response['ipv6']),
    ]
    
    message.extend(['', 'DNS Updates'])
    for dns in response['dns']:
        ipv4_message = update_text(dns['ipv4_update'], dns['ipv4_update_success'])
        ipv6_message = update_text(dns['ipv6_update'], dns['ipv6_update_success'])
        message.extend([
            '    {} A: {}'.format(dns['target_domain'], ipv4_message),
            '    {} AAAA: {}'.format(dns['target_domain'], ipv6_message),
        ])
    
    message.extend(['', 'Firewall Updates'])
    for firewall in response['firewall']:
        ipv4_message = update_text(firewall['ipv4_update'], firewall['ipv4_update_success'])
        ipv6_message = update_text(firewall['ipv6_update'], firewall['ipv6_update_success'])
        message.extend([
            '    {} ({}) IPv4: {}'.format(firewall['name'], firewall['port'], ipv4_message),
            '    {} ({}) IPv6: {}'.format(firewall['name'], firewall['port'], ipv6_message),
        ])
    
    
    # Send report
    send_email(
        {
            'name': 'Signpost ({}) <{}>'.format(config['sender_tag'], config['sender_address']),
            'arn': config['sender_arn'],
        },
        config['recipient'],
        'Signpost Update FAILED' if has_failures else 'Signpost Update Report',
        message,
    )
    

def handler(event, context = {}):
    
    # Load config and data
    config = load_config()
    
    new_ipv4 = event.get('ipv4', None)
    new_ipv6 = event.get('ipv6', None)
    
    
    # Prepare outcome container
    status = {'ipv4': new_ipv4, 'ipv6': new_ipv6,'dns': [], 'firewall': []}
    
    # Perform DNS updates
    for dns_config in config.dns:
        status['dns'].append(dns_receiver(
            dns_config['domain'],
            dns_config['subdomain'],
            new_ipv4,
            new_ipv6,
        ))
    
    
    # Perform firewall updates
    for firewall_config in config.firewall:
        status['firewall'].append(firewall_receiver(
            firewall_config,
            new_ipv4,
            new_ipv6,
        ))
    
    
    # Email reporting
    for email_config in config.email:
        email_reporter(email_config, status)
    
    # Return results
    return status

