# Signpost
Signpost is a free, self-managed dynamic DNS and firewall service, for use with AWS infrastructure.
The system is comprised of two parts:

* Server-side deployment on AWS Lambda, which has access to other AWS resources.
* Client-side deployment either using Docker or running on bare-metal.

To run Signpost, you will need a domain with DNS provided by AWS Route53 and/or AWS EC2 instances with firewalls managed through Security Groups.

### Why use Signpost?
AWS permissions for Route53 are only granted at the domain level, and permissions for firewalls are only granted at the Security Group level.
To update these records directly, the client would therefore need _full_ DNS or firewall write access for the respective resources.
Obviously, this presents a large security risk, and so the minimalist server on Lambda provides a secure bridge between the two.


## Server Deployment
A Signpost server consists of a Lambda function, which has the ability to update various other AWS resources through the configuration of _receivers_.
The Lambda itself should be set up as follows:

* Runtime: Python 3.8
* Hander: `server.handler`
* Signpost files: `server.py` and `aws.py`
* Role: Has `AWSLambdaBasicExecutionRole` policy attachment

The system can then be configured with through the `config.py` file, based on the template provided.
See the sections below for the receivers available, as well as their specific requirements and configurations.


### DNS Receivers
Signpost can act as a dynamic DNS service and update Route53 records based on received data.
Multiple target domains can be set across multiple hosted zones, and both IPv4 (`A`) and IPv6 (`AAAA`) records are updated.
To leverage this functionality, you require:

* Route53 hosted zone(s) which contains the target domain name(s) - They do not need to be the root record.
* Additional permissions on the server execution role:
    * `Route53/ListHostedZones`
    * `Route53/ListResourceRecordSets` - Limited to required hosted zone(s)
    * `Route53/ChangeResourceRecordSets` - Limited to required hosted zone(s)
* A `dns` variable in `config.py`, formatted as follows:  
    `[{'domain': 'example.org', 'subdomain': 'local'}, ...]`
    * `domain` The main domain of the hosted zone.
    * `subdomain` The subdomain to record the dynamic DNS information.


### Firewall Receivers
Signpost can provide dynamic firewall management and update access to EC2 instances based on received data.
Rules for multiple ports can be set across multiple security groups, and both IPv4 and IPv6 rules are updated.
Multiple servers can also manage rules on the same security group/port combination.
In this configuration, optional tags should be used to prevent servers from overriding each other's access rules.
To leverage this functionality, you require:

* Security group(s) which will provide the dynamic access to your EC2 instances.
* Additional permissions on the server execution role:
    * `ec2/DescribeSecurityGroups`
    * `ec2/RevokeSecurityGroupIngress` - Limited to required security group(s)
    * `ec2/AuthorizeSecurityGroupIngress` - Limited to required security group(s)
* A `firewall` variable in `config.py`, formatted as follows:  
    `[{'id': 'sg-xxxxxxxxxxxxxxxxx', 'name': 'Example', 'port': 2222, 'tag': 'Server'}, ...]`
    * `id` _(Either)_ The ID of the security group.
    * `name` _(Or)_ The group name of the security group.
    * `port` The port number to manage rules on.
    * `tag` _(Optional)_ Additional text to identify the managing server.


### Email Reporters
Once the updates have been performed, Signpost can send an update report by e-mail.
This report contains the new IP addresses of the client, as well as the update status of any receivers.
No report is sent if no updates are attempted.
To leverage this functionality you require:

* A registered SES domain or e-mail address in the `eu-west-1` region.
* Additional permissions on the server execution role:
    * `ses:SendEmail` & `ses:SendRawEmail` - Limited to the required sender(s).  
    Optionally, conditional restriction(s) on recipients and specific sender addresses can also be applied.
* An `email` variable in `config.py`, formatted as follows:  
    `[{'sender_tag': 'Server', 'sender_address': 'admin@example.com', 'sender_arn': 'arn:...', 'recipient': 'user@example.org'}, ...]`
    * `sender_tag` An identifier for the server sending the report.
    * `sender_address` The address the report should be sent from.
    * `sender_arn` The ARN of the SES sender address or domain.
    * `recipient` The address the report should be sent to.


_N.B. If the sender domain/address is still sandboxed, the recipient should also be registered with SES and the server should also have sending permissions from them._


## Client Deployment
There is flexibility about how the Signpost client can be deployed, although the recommended method (and how we do it ourselves) is inside a Docker container.

Irregardless, an IAM User, with Lambda Invoke permissions for the server and remote access keys, is required.

The client should be run on regular intervals to ensure records are kept up-to-date.
On Linux, this can be achieved through a cronjob, which can be configured by calling `crontab -e`.

### in Docker
A Docker image for the client is available on [Docker Hub](https://hub.docker.com/r/joehitchen/signpost) as `joehitchen/signpost`.  

This image is automatically updated whenever changes are made to the project.
The recommended invocation is:  
  
```
    docker run --network=host \
        -e "SIGNPOST_LAMBDA=<my-signpost-function>" \
        -v /path/to/aws/config:/home/signpost/.aws/config:ro \
        joehitchen/signpost
```
A full explanation of this can be found on the Docker Hub page.  

Alternatively, the image can be built locally from the source code using the `Dockerfile` provided.
The usage is exactly the same as the public image.

### on bare-metal
The client can also be run as a python script on the client machine.
After copying the source code, the process for this is:

1. Create a virtualenv and install the dependencies.
1. Set up AWS environment (either by environment variables, session variables, or the `.aws` directory.
1. Set `SIGNPOST_LAMBDA` as an environment (or session) variable.
1. Call `python client.py`.

_Signpost is anticipated to work with Python >=3.5, and may work on older versions, but is only verified on Python 3.8.
If in doubt, the test suite can be run after installing the dependencies by calling `python -m unittest`._
