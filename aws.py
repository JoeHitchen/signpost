import boto3
import json
import logging


class Route53SubDomain():
    
    def create_default_record(self, type):
        return {
            'Name': self.fqdn,
            'Type': type,
            'ResourceRecords': [{'Value': None}],
            'TTL': 300,
        }
    
    def __init__(self, domain, subdomain, **kwargs):
        
        # Process inputs
        self.domain = domain + ('.' if not domain[-1] == '.' else '')
        
        if subdomain[-1] == '.':  # Strip training .
            subdomain = subdomain[:-1]
        if self.domain[:-1] in subdomain:  # Strip domain
            subdomain = subdomain[:-len(self.domain[:-1])-1]
        self.subdomain = subdomain
        
        self.fqdn = subdomain + '.' + self.domain
        
        # Load data
        self._client = boto3.client('route53', **kwargs)
        
        self._zone = {
            zone['Name']: zone
            for zone in self._client.list_hosted_zones()['HostedZones']
        }[self.domain]
        self._zone_id = self._zone['Id']
        
        
        zone_records = self._client.list_resource_record_sets(
            HostedZoneId = self._zone_id
        )['ResourceRecordSets']
        
        self._type_map = {
            record['Type']: record
            for record in zone_records
            if record['Name'] == self.fqdn
        }
    
    
    @property
    def ipv4(self):
        return self.getter('A')
    
    @ipv4.setter
    def ipv4(self, ipv4):
        self.setter('A', ipv4)
    
    
    @property
    def ipv6(self):
        return self.getter('AAAA')
    
    @ipv6.setter
    def ipv6(self, ipv6):
        self.setter('AAAA', ipv6)
    
    
    def getter(self, type):
        record = self._type_map.get(type, None)
        return record['ResourceRecords'][0]['Value'] if record else None
    
    
    def setter(self, type, value):
        
        if value == self.getter(type):
            logging.info('No change needed for {} record of {}'.format(type, self.fqdn))
            return
        
        logging.info('Changing {} record of {} from {} to {}'.format(
            type, self.fqdn, self.getter(type), value,
        ))
        
        if type not in self._type_map:
            self._type_map[type] = self.create_default_record(type)
        
        self._type_map[type]['ResourceRecords'][0]['Value'] = value
        
        self._client.change_resource_record_sets(
            HostedZoneId = self._zone_id,
            ChangeBatch = { 'Changes': [{
                'Action': 'UPSERT',
                'ResourceRecordSet': self._type_map[type],
            }] }
        )



class FirewallPort():
    
    def __init__(self, config):
        self.port = int(config['port'])
        
        group_id = config.get('id', None)
        if not group_id:
            ec2_client = boto3.client('ec2')
            groups = ec2_client.describe_security_groups(GroupNames = [config.get('name')])
            group_id = groups['SecurityGroups'][0]['GroupId']  # Unable to get double return
        
        self._sg = boto3.resource('ec2').SecurityGroup(group_id)
        
        self.id = self._sg.group_id
        self.name = self._sg.group_name
    
    
    @property
    def ipv4_rules(self):
        return [
            rule
            for permission in self._sg.ip_permissions
            for rule in permission['IpRanges']
            if permission['FromPort'] == permission['ToPort'] == self.port
        ]
    
    @property
    def ipv6_rules(self):
        return [
            rule
            for permission in self._sg.ip_permissions
            for rule in permission['Ipv6Ranges']
            if permission['FromPort'] == permission['ToPort'] == self.port
        ]
    
    
    def add_cidr(self, cidr_range, description):
        
        ipv6 = ':' in cidr_range
        ip_range_key = 'Ipv6Ranges' if ipv6 else 'IpRanges'
        cidr_ip_key = 'CidrIpv6' if ipv6 else 'CidrIp'
        
        self._sg.authorize_ingress(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            ip_range_key: [{cidr_ip_key: cidr_range, 'Description': description}],
        }])
    
    
    def revoke_multiple_cidr(self, cidr_ranges):
        
        if not cidr_ranges:
            return
        
        ipv6 = ':' in cidr_ranges[0]
        ip_range_key = 'Ipv6Ranges' if ipv6 else 'IpRanges'
        cidr_ip_key = 'CidrIpv6' if ipv6 else 'CidrIp'
        
        self._sg.revoke_ingress(IpPermissions = [{
            'FromPort': self.port,
            'ToPort': self.port,
            'IpProtocol': 'tcp',
            ip_range_key: [{cidr_ip_key: cidr_range} for cidr_range in cidr_ranges]
        }])


def invoke_lambda(name, data = {}):
    
    response = boto3.client('lambda').invoke(
        FunctionName = name,
        Payload = json.dumps(data),
    )
    
    raw_response_data = response['Payload'].read()
    return json.loads(raw_response_data.decode())


def send_email(sender, recipient, subject, message):
    
    message = '\n'.join(message) if type(message) == list else message
    
    boto3.client('ses', region_name = 'eu-west-1').send_email(
        Source = sender['name'],
        SourceArn = sender['arn'],
        Destination = {'ToAddresses': [recipient]},
        Message = {
            'Subject': {'Data': subject},
            'Body': {'Text': {'Data': message}},
        },
    )

