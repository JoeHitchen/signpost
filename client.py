import os
import requests
from ipaddress import IPv4Address, IPv6Address, AddressValueError

from aws import invoke_lambda


def ipify(ipv6 = False):
    host = 'api64.ipify.org' if ipv6 else 'api.ipify.org'
    response = requests.get('https://{}?format=json'.format(host))
    
    if not response.status_code == 200:
        print('IPify {}: Bad response'.format('IPv6' if ipv6 else 'IPv4'))
        return None
    
    ip_string = response.json()['ip']
    
    try:
        ip_address = IPv6Address(ip_string) if ipv6 else IPv4Address(ip_string)
        print('IPify {}: {}'.format('IPv6' if ipv6 else 'IPv4', ip_address))
        return str(ip_address)  # str required for serialisation
    
    except AddressValueError:
        print('IPify {}: Invalid'.format('IPv6' if ipv6 else 'IPv4'))
        return None
    

if __name__ == '__main__':
    
    response_data = invoke_lambda(
        os.environ['SIGNPOST_LAMBDA'],
        {'ipv4': ipify(), 'ipv6': ipify(ipv6 = True)},
    )
    
    print(response_data)

